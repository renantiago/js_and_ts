function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  const nameError = 'validation error: invalid name size';
  const emailError = 'validation error: invalid email format';
  const passwordError = 'validation error: invalid password format';
  const passConfirmationError = 'validation error: confirmation does not match';

  test('given valid user should create it', () => {
    expect(() => {
      createUser('name', 'a@a.com.br', 'password', 'password')
    }).not.toThrow()
  });

  test('given user with invalid email should throw error', () => {
    expect(() => {
      createUser('name', 'invalid_email', 'password', 'password')
    }).toThrow(emailError)
  });

  test('given user with empty name should throw error', () => {
    expect(() => {
      createUser('', 'a@a.com.br', 'password', 'password')
    }).toThrow(nameError)
  });

  test('given user with name size greater than 64 with should throw error', () => {
    expect(() => {
      createUser('a'.repeat(65), 'a@a.com.br', 'password', 'password')
    }).toThrow(nameError)
  });

  test('given user with invalid password with should throw error', () => {
    expect(() => {
      createUser('name', 'a@a.com.br', '_=+!', 'password!')
    }).toThrow(passwordError)
  });

  test('given user with wrong password confirmation with should throw error', () => {
    expect(() => {
      createUser('name', 'a@a.com.br', 'password', 'password123')
    }).toThrow(passConfirmationError)
  });
});
