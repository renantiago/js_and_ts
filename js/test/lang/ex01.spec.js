function objectFromArrays(keys, values) {
  const obj = {}
  keys.forEach((key, index) => obj[key] = values[index])
  return obj
}

test('Creating objects from arrays', () => {
  const keys = ['name', 'email', 'username'];
  const values = ['Bruno', 'bruno@mail.com', 'bruno'];

  const finalObject = {
    name: 'Bruno',
    email: 'bruno@mail.com',
    username: 'bruno'
  };

  expect(objectFromArrays(keys, values)).toEqual(finalObject);
});

function reverse(a, b) {
  if (a > b) {
    return -1;
  }
  if (b > a) {
    return 1;
  }
  return 0;
}

function indexer(item, index) {
  return `${index + 1}. ${item}`
}

function shorterThan6(string) {
  return string.length < 6
}

describe('array functions', () => {
  test('reversed indexed arrays', () => {
    const names = ['Marina', 'Camila', 'Alberto', 'Felipe', 'Mariana'];

    const test = names.sort(reverse).map(indexer);
    const correct = [
      '1. Marina',
      '2. Mariana',
      '3. Felipe',
      '4. Camila',
      '5. Alberto'
    ];
    expect(test).toEqual(correct);
  });

  test('filtering lists', () => {
    const techComps = [
      'microsoft',
      'google',
      'apple',
      'ibm',
      'amazon',
      'facebook'
    ];

    const shortNames = techComps.filter(shorterThan6);
    const correctShortNames = ['apple', 'ibm'];

    expect(shortNames).toEqual(correctShortNames);
  });
});
