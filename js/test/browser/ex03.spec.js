const test = require('ava');

function compare(a, b) {
  if (a.name > b.name) {
    return 1;
  }
  if (b.name > a.name) {
    return -1;
  }
  return 0;
}

function scaffoldStructure(document, data) {
  const ordered_data = data.sort(compare);
  const list = document.createElement('ul');
  ordered_data.forEach(item => {
    let li = document.createElement('li');
    li.innerHTML = `<b class='name'>${item.name}</b> - <a href='http://${item.email}'>${item.email}</a>`
    list.appendChild(li)
  });
  document.body.appendChild(list)
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);
});

test('should have an unordered list', t => {
  const ulNodes = document.querySelectorAll('ul');
  
  t.assert(ulNodes.length > 0)
});

test('should have elements with name css class', t => {
  const nameNodes = document.querySelectorAll('.name');
  
  t.assert(nameNodes.length > 0)
});

test('data list should be alphabetically ordered', t => {
  const liNodes = document.querySelectorAll('li');
  let liTexts = [];
  
  liNodes.forEach(node => {
    liTexts.push(node.textContent)
  });
  
  let orderedLiTexts = liTexts.sort()
  
  t.is(liTexts.toString, orderedLiTexts.toString)
});

test('data list item should have bold name and link to email', t => {
  const liNode = document.querySelector('li');
  
  t.is(liNode.innerHTML, '<b class="name">Bernardo</b> - <a href="http://b.ern@mail.com">b.ern@mail.com</a>')
});